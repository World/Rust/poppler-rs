# poppler-rs

A high-level (safe) set of Rust bindings for
[poppler](https://poppler.freedesktop.org/)'s glib interface.

poppler is a PDF rendering library with a cairo backend.

## Usage

There's no nice tutorial or sample code for now: check out the rustdoc to see
what types are available.

But generally, usage involves:

  * Creating a `Document`, maybe from a `&[u8]` slice.
	* Enumerating pages, rendering them to a cairo surface
