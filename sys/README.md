# poppler-sys-rs

You probably want to use `poppler-rs` instead, as this crate only exists to
provide FFI with poppler-glib.

See <https://gitlab.gnome.org/World/Rust/poppler-rs> for more information.
