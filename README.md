# poppler-rs

Work-in-progress Rust bindings for [poppler](https://poppler.freedesktop.org/), the PDF rendering library.

The bindings (`poppler-sys` and `poppler`) are generated with
[gir](https://github.com/gtk-rs/gir): most of the work is writing `Gir.toml` files,
and tuning the resulting `Cargo.toml` file.

Website: <https://world.pages.gitlab.gnome.org/Rust/poppler-rs>

## Documentation

- poppler: <https://world.pages.gitlab.gnome.org/Rust/poppler-rs/stable/latest/docs/poppler>
- poppler-sys: <https://world.pages.gitlab.gnome.org/Rust/poppler-rs/stable/latest/docs/poppler_sys>

## Dependencies

This binding is for the "glib" interface to poppler, and as such, depends on
`cairo`, `gio`, `glib`, and `gobject`.

See `(cd sys && cargo tree)` for details.

## Project structure

This repository is a cargo workspace with two members:

  * the `sys` directory contains the `poppler-sys-rs` crate (package `poppler-sys`)
  * the `poppler` directory contains the `poppler-rs` crate (package `poppler`)

## Version number

gir picked `0.18.0` as an initial version number for `poppler-sys-rs` - I'm
assuming that's the version of `poppler-glib`. I aligned `poppler-rs`'s version
number with that.

Over time the version numbers will probably get out of sync with upstream.

## Gir files / bindings generation

`just gir` (see `Justfile`) regenerates bindings for both crates.

The <https://github.com/gtk-rs/gir-files> repo should be present on disk as a
sibling of this repository.

The `Poppler*.gir` file contained in `poppler-rs/gir-files` was pulled from a
handmade build of poppler on Fedora 35, it's unfortunately not included in the
Ubuntu `libpoppler-dev` package.
